

function storageMsg(jid_storage,from_jid,msg,date_sent){
	var storedMsg = [];
	if (localStorage.getItem(jid_storage) !== null) {
	  var storedMsg = JSON.parse(localStorage[jid_storage]);
	}
	from_jid = from_jid.split('/')[0];
	var objMsg = {id:from_jid,message:msg,date:date_sent};
	storedMsg.push(objMsg);
	localStorage.setItem(jid_storage,JSON.stringify(storedMsg));
}

function loadStorageMsg(jid){
	var storedMsg = [];
	if (localStorage.getItem(jid) !== null) {
	  var storedMsg = JSON.parse(localStorage[jid]);
	}
	return storedMsg;
}

function clearHistoryStorageMsg(jid){
	showAlert('HappyMe','Đã xóa toàn bộ lịch sử trò chuyện.');
}

function alertDismissed() {
    // do something
}

function showAlert(title,msg) {
	navigator.notification.alert(
		msg,  // message
		alertDismissed,         // callback
		title,            // title
		'OK'                  // buttonName
	);
}

function showPrompt() {
    navigator.notification.prompt(
        'Please enter your name',  // message
        alertDismissed                  // callback to invoke
    );
}

function addMessage(contact) {
    
}


// handle window resize
$(window).resize(function() {
    $('.message-input').width($(window).width() - 100);
});

