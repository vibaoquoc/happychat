var io = require("socket.io");

var socket = io.listen(8080, "127.0.0.1");

socket.set("log level", 1);
var people = {};
var maps = [];
var databaseUrl = "sampleDB"; // "username:password@example.com/mydb"
var collections = ["users", "reports"];
var db = require("mongojs").connect(databaseUrl, collections);

function getInfoByUsername(username){
	console.log(maps);
	console.log(username);
	maps.forEach(function(user){
		if (user.username == username){
			return user;
			console.log(user);
		}
	});
}

function removeInfoByUsername(username){
	console.log('remove '+username);
	for(i=0;i<maps.length;i++){
		if (map[i].username == username){
			maps.splice(i,1);
		}
	}
}

socket.on("connection", function (client) {
	client.on("join", function(username){
		maps.push({
			username: username,
			client_id: client.id, 
			display_name:'', 
			avatar:'',
			status:''  
		});

		db.users.find({}, function(err, users) {
			console.log('mongojs');
			console.log(users);
		});

		db.users.find({username: username}, function(err, users) {
			if( err || !users) { 
				db.users.save({username: username, display_name: "", avatar: "", status:"",last_active: new Date() }, function(err, saved) {
			  		if( err || !saved ) console.log("User not saved");
			  		else console.log("User saved");
				});
			}
			else users.forEach( function(user) {
				db.users.update({username: username}, {$set: {last_active: new Date()}}, function(err, updated) {
					if( err || !updated ) console.log("User not updated");
					else console.log("User updated");
				});
			});
		});

		console.log(maps);
		people[client.id] = username;
		client.emit("update", "You have connected to the server.ID:" + client.id);
		socket.sockets.emit("update", username + " has joined the server.")
		socket.sockets.emit("update-people", people);
	});

	client.on("send", function(msg){
		console.log(msg);
		maps.forEach(function(user){
			console.log(user);
			if (user.username == msg.msg_to){
				socket.sockets.socket(user.client_id).emit("chat", people[client.id], msg);
				return;
			}
		});
	});

	client.on("disconnect", function(){
		socket.sockets.emit("update", people[client.id] + " has left the server.");
		delete people[client.id];
		
		for(i=0;i<maps.length;i++){
			if (maps[i].client_id == client.id){
				maps.splice(i,1);
			}
		}
		
		socket.sockets.emit("update-people", people);
	});
});


