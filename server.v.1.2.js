var io = require("socket.io"),
	crypto = require('crypto');

var socket = io.listen(8080, "localhost");
console.log('socket.io listen on port 8080');

socket.set("log level", 1);
//socket.set("origins","*:*");
socket.set('transports', [ 'websocket', 'xhr-polling', 'jsonp-polling']);

//Danh sách người trong phòng chat
var people = {};
var colors = ["#1E90FF","#FF7F50","#0000FF","#5F9EA0","#DAA520","#9ACD32","#00FF7F","#D2691E","#FF69B4","#B22222","#8A2BE2","#2E8B57","#008000"];
var people_color = {};

//Privatekey dùng để xác thực dữ liệu gởi lên
var pk = "das8189q54asd@022kka&@#";

//Danh sách user bị cấm
var users_banned = [];

//Danh sách user bị tạm ngưng 10phut
var users_pending = [];

//Danh sách moderator quản lý phòng này
var moderators = ['vibaoquoc@gmail.com'];

//Danh sách user có quyền add moderator
var supperUsers = ['vibaoquoc@gmail.com','hiruto'];

var visitedCount = 0;
var objconfig = {
	maxlength : 200
};

socket.on("connection", function (client) {
	visitedCount++;
	console.log('visitedCount: ' + visitedCount);
	client.on("join", function(name,signature){
		/*
			Tham gia phòng chat chat - user đã đăng nhập Gate
		*/
		//console.log(name+'_'+signature);
		if (crypto.createHash('md5').update(name+pk).digest("hex") == signature){
			people[client.id] = name;
			people_color[client.id] = colors[Math.floor((Math.random() * 10) + 1)];
			client.emit("update", "Chào mừng bạn tham gia Gate TV.");
			//socket.sockets.emit("update", name + " vừa vào phòng GateTV.");
			socket.sockets.emit("update-people", people);	
			// console.log(people);
			// console.log(people_color);
		}else{
			console.log(name+'_'+signature);
		}
	});

	client.on("send", function(msg){
		/*
			Gởi tin nhắn vào room
			Check user bị ban thì sẽ không được gởi tin nhắn
		*/
		if (users_banned.indexOf(people[client.id]) > -1)
			return;
		msg = msg.substring(0, objconfig.maxlength);
		//console.log(people[client.id] +'_'+ msg); 
		socket.sockets.emit("chat", people[client.id], msg, people_color[client.id] );
	});

	client.on("disconnect", function(){
		/*
			Khi user unfocus trình duyệt hay đóng trình duyệt
		*/
		//socket.sockets.emit("update", people[client.id] + " vừa rời khỏi phòng GateTV.");
		delete people[client.id];
		delete people_color[client.id];
		socket.sockets.emit("update-people", people);
	});

	client.on("listmoderator", function(){
		/*
			Trả về danh sách mod của room
		*/
		socket.sockets.emit("listmoderator", moderators);
	});

	client.on("setcolor", function(data){
		/*
			Thay đổi màu của user chat
		*/
		if (colors.indexOf(data) > -1)
			people_color[client.id] = data;
	});

	client.on("bannick",function(user_banned,type_banned,signature){ //type_banned:1=ban,0:unban
		var moderator = people[client.id];
		/*
			Kiểm tra user action có phải là moderator
			Kiểm tra chữ ký hợp lệ
			Kiểm tra user có bị ban chưa
		*/
		if (moderators.indexOf(moderator) > -1){
			var position_user_banned = users_banned.indexOf(user_banned);
			if (position_user_banned == -1){
				//Chưa có mới ban được
				if (position_user_banned == -1){
					users_banned.push(user_banned);	
				}
			}
			else{
				//Có rồi mới unban được
				if (position_user_banned > -1){
					users_banned.splice(position_user_banned, 1);
				}
			}
		}
	});

	client.on("addmoderator",function(moderator,signature){
		/*
			Chi những user đặc biệt mới được set những user khác làm mod
		*/
		if (supperUsers.indexOf(people[client.id]) > -1){
			moderators.push(moderator);
			socket.sockets.emit("listmoderator", moderators);
		}
	});
});
